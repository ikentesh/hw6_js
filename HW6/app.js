"use strict";

/*Теоретичні питання

Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.

Екранування - це спосіб розміщення всередині літералу 
символів закінчення літералу, а також спеціальних символів. Прикладом використання екранування може бути додавання в рядкові літерали одиночні
та подвійні лапки шляхом вставки символу \ (зворотний слеш) перед символом, що екранується.

Які засоби оголошення функцій ви знаєте? 

Function declaration - оголошення функції складається з ключового слова function, за яким слідує  обов'язкове ім'я функції, 
списку параметрів у круглих дужках (...) та парою фігурних дужок {...}.
Function expression - Вираз функції визначається ключовим словом function, 
за яким слідує необов'язкове ім'я функції, списком параметрів у круглих дужках (...) і парою фігурних дужок {...}.

Що таке hoisting, як він працює для змінних та функцій? 

hoisting - це механізм JavaScript, в якому змінні і оголошення функцій, пересуваються вгору своєї області видимості 
перед тим, як код буде виконано. Це означає те, що зовсім неважливо де були оголошені функція або змінні, всі вони пересуваються 
вгору своєї області видимості, незалежно від того, локальна вона або глобальна.*/

// Завдання

function createNewUser() {
  let firstName = prompt("Enter your name.");
  let lastName = prompt("Enter your surname.");
  let birthday = prompt("Enter your birth date (dd.mm.yyyy).");
  return {
    firstName,
    lastName,
    birthday,

    getAge: function () {
      let currentYear = new Date().getFullYear();
      let birthYear = this.birthday.split(".");
      let userAge = currentYear - birthYear[2];
      return "Age:" + userAge;
    },

    getLogin: function () {
      return (
        "Login:" +
        this.firstName.charAt(0) +
        this.lastName
      ).toLowerCase();
    },

    getPassword: function () {
      let birthYear = this.birthday.split(".");
      return (
        "Password:" +
        this.firstName[0].toLocaleUpperCase() +
        this.lastName.toLowerCase() +
        birthYear[2]
      );
    },
  };
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getLogin());
console.log(newUser.getPassword());
